//? future
// void main() {
//   fetchUserOrder();
//   print('Ambil Datanya');
// }

// Future<void> fetchUserOrder() {
//   return Future.delayed(Duration(seconds: 2),
//       () => print('Selamat datang Peserta bootcamp flutter'));
// }

void main(List<String> args) async {
  // * Future delayed
  print('Life');
  printMotto('never flat');
  print('is');

//   // * ( Async, Await and future )
  print('Ready, Sing');
  await line();
  await line2();
  await line3();
  await line4();
}

Future line() async {
  await Future.delayed(const Duration(seconds: 5));
  print("pernahkan kau merasa");
}

Future line2() async {
  await Future.delayed(const Duration(seconds: 3));
  print("pernahkah kau merasa ...........");
}

Future line3() async {
  await Future.delayed(const Duration(seconds: 2));
  print("pernahkah kau merasa");
}

Future line4() async {
  await Future.delayed(const Duration(seconds: 1));
  print("hatimu hampa pernahkah kau merasa hati mu kosong ............");
}

printMotto(String motto) async {
  await Future.delayed(const Duration(seconds: 1));
  print(motto);
}
