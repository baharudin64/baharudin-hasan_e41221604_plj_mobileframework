import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(const Home());
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<String> gambar = [
    "chimmy.gif",
    "cooky.gif",
    "koya.gif",
    "mang.gif",
    "rj.gif",
    "shooky.gif",
    "tata.gif",
    "van.gif",
  ];

  static const Map<String, Color> colors = {
    'chimmy': Color(0xff2DB569),
    'cooky': Color(0xffF386B8),
    'koya': Color(0xff45CAF5),
    'mang': Color(0xffB19ECB),
    'rj': Color(0xffF58E4C),
    'shooky': Color(0xff46C1BE),
    'tata': Color(0xffFFEA0E),
    'van': Color(0xffDBE4E9),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              colors: [Colors.white, Colors.purple, Colors.deepPurple],
            ),
          ),
          child: PageView.builder(
            controller: PageController(viewportFraction: 0.8),
            itemBuilder: (BuildContext context, int i) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                child: Material(
                  elevation: 8.0,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Hero(
                        tag: gambar[i],
                        child: Material(
                          child: InkWell(
                            // flex: 1,
                            child: Flexible(
                              child: Container(
                                color: colors.values.elementAt(i),
                                child: Image.asset(
                                  "assets/img/${gambar[i]}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Halamandua(
                                    gambar: gambar[i],
                                    colors: colors.values.elementAt(i),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class Halamandua extends StatelessWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BT21'),
        backgroundColor: Colors.purpleAccent,
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: RadialGradient(
                  center: Alignment.center,
                  colors: [Colors.purple, Colors.white, Colors.deepPurple]),
            ),
          ),
          Center(
            child: Hero(
              tag: gambar,
              child: ClipOval(
                child: SizedBox(
                  width: 200.0,
                  height: 200.0,
                  child: Material(
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Flexible(
                        // flex = 1;
                        child: Container(
                          color: colors,
                          child: Image.asset(
                            'assets/img/$gambar',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// class _HomeState extends State<Home> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Container(
//           decoration: const BoxDecoration(
//             gradient:
//                 LinearGradient(begin: FractionalOffset.topCenter, colors: [
//               Colors.white,
//               Colors.purpleAccent,
//               Colors.deepPurple,
//             ]),
//           ),
//         ),
//       ),
//     );
//   }
// }
