// import 'dart:io';

// void main(List<String> args) {
//   print("masukan password: ");
//   String inputText = stdin.readLineSync()!;
//   print("password: " + inputText.toString());
// }

void main() {
  var name = "John"; // Tipe
  var angka = 12;
  var todayIsFriday = false;
  print(name); // "John"
  print(angka); // 12
  print(todayIsFriday); // false
  var items;
  print(items); //null

  // var angka = 100;
  // print(angka == 100); // true
  // print(angka == 20); // false

  // var sifat = "rajin";

  // print(sifat != "malas"); // true
  // print(sifat != "bandel"); //true
  // print(sifat == "rajin"); //true
  // print(sifat != "rajin"); //false

  // var angka = 8;
  // print(angka == "8"); // true, padahal "8" adalah string.
  // print(angka === "8"); // false, karena tipe data nya berbeda
  // print(angka === 8); // true

  // var number = 17;
  // print(number < 20); // true
  // print(number > 17); // false
  // print(number >= 17); // true, karena terdapat sama dengan
  // print(number <= 20); // true
  // print(number >= 20);

// //OR
//   print(true || true); // true
//   print(true || false); // true
//   print(true || false || false); // true
//   print(false || false); // false

//   // AND
//   print(true && true); // true
//   print(true && false); // false
//   print(false && false); // false
//   print(false && true && true); // false
//   print(true && true && true); // true

  // var sentences = "dart";
  // print(sentences[0]); // "d"'
  // print(sentences[1]);
  // print(sentences[2]); // "r"
  // print(sentences[3]);

// //number
//   // declare an integer
//   int num1 = 10;
//   // declare a double value
//   double num2 = 10.50;
//   // print the values
//   print(num1); //10
//   print(num2); //10.5

//   print(num.parse('12')); //12
//   print(num.parse('10.91')); //10.91

//   int j = 45;
//   String t = "$j";
//   print("hello" + t);
}
